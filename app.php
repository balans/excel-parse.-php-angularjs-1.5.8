<?php

// included config.
require_once ('config.php');

// For development
function die_by_error($file, $line, $msg = null) {
    $str = "<h1>We have a error</h1>"
        . "<p>File: <strong>{$file}</strong></p>"
        . "<p>Line: <strong>{$line}</strong></p>";

    if (!isset($msg))
        $str .= '<p>Message: {$msg}</p>';

    die($str);
}

// Autoload classes
function __autoload($class) {
    // convert namespace to full file path
    $class = APP_ROOT . '/src/' . str_replace('\\', '/', $class) . '.php';
    if (file_exists($class)) {
        require_once($class);
    }
}

function myEcho($strOrObj, $isDie = false) {
    echo '<pre>';
    print_r($strOrObj);
    echo '</pre>';

    if ($isDie) {
        die('die');
    }
}
