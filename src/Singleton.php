<?php

class Singleton {
    protected static $_db;
    protected static $_isStartSession = false;
    protected static $_excelReader = null;
    private function __construct() {}
    private function __clone() {}
    private function __wakeup() {}

    public static function initDbConnect() {
        if ( is_null(self::$_db) ) {
            self::$_db = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
        }
        return self::$_db;
    }

    public static function startSession() {
        if ( !self::$_isStartSession ) {
            self::$_isStartSession = true;
            session_start();
        }
    }

    public static function getExcelReader($filePath) {
        if ( is_null(self::$_excelReader) ) {
            require_once APP_ROOT . '/vendor/Classes/PHPExcel.php';

        }
        self::$_excelReader = PHPExcel_IOFactory::createReaderForFile($filePath);
        return self::$_excelReader;
    }

    public static function removeSession() {
        self::startSession();
        session_destroy();
    }
}