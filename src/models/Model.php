<?php

namespace models;

class Model
{
    protected $_dbConnect = null;

    function __construct() {
        $this->_dbConnect = \Singleton::initDbConnect();
    }

    public function beginTransaction() {
        $this->_dbConnect->beginTransaction();
    }

    public function transRollback() {
        $this->_dbConnect->rollback();
    }

    public function transCommit() {
        $this->_dbConnect->commit();
    }

    public function writeRows($rows, $mapping) {
        $values = [];

        foreach ($rows as $key => $row) {
            $values[] = "('" . implode("', '", $row) . "')";
        }

        $cols = '(`' . implode('`, `', $mapping) . '`)';
        $vals = implode(', ' , $values );

        $sql = "INSERT INTO `employees` {$cols} VALUES {$vals}";

        $stmt = $this->_dbConnect
            ->prepare($sql);
        return $stmt->execute();
    }
}