<?php

namespace ctrl;

use \models\Model;

class Base
{
    protected $_apiData = null;

    public function __construct() {
        $this->_setModel();
        $this->_initApiData();
    }

    protected function _setModel() {
        $this->_model = new Model();
    }

    protected function _initApiData() {
        $temp = new \stdClass();
        $temp->file_name = null;
        $temp->show_rows = null;
        $temp->password  = null;
        $temp->status    = null;
        $temp->mapping   = null;
        $temp->file_extension = null;
        $temp->sys_file_name  = null;
        $temp->allowed_cols = null;

        $this->_apiData = $temp;
    }

    public function getApiData() {
        return $this->_apiData;
    }
}