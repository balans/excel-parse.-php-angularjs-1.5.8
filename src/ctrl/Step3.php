<?php

namespace ctrl;

class Step3 extends \ctrl\Base
{
    protected $_api_data = null;
    protected $_errorMsg = null;

    public function __construct($api_data) {
        parent::__construct();
        $this->_api_data = $api_data;

        $this->notRequiredCells = [
            'country' => true,
            'city' => true,
            'address' => true,
            'password' => true
        ];
    }

    public function getErrorMsg() {
        return $this->_errorMsg;
    }

    public function writeDataToDb() {
        $tempStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $file = $this->_api_data->sys_file_name;
        $excelReader = \Singleton::getExcelReader($file);
        $excelObj = $excelReader->load($file);
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow();

        $this->_model->beginTransaction();

        $temp = [];
        $isOk = true;
        $mapping = $this->_api_data->mapping;
        $mapping[] = 'status';
        try {
            for ($row = 1; $row <= $lastRow; $row++) {

                if (0 == $row % 100) {
                    $isOk = $this->_model->writeRows($temp, $mapping);
                    if (!$isOk) {
                        $this->_errorMsg = 'Sorry, but the system has a DB error.';
                        break;
                    }
                    $temp = [];
                }

                for ($i = 0; $i < count($this->_api_data->mapping); $i++) {
                    $temp[$row][$i] = $worksheet->getCell($tempStr[$i] . $row)->getValue();
                }

                if( !$this->_checkRequiredCells($temp[$row], $this->_api_data->password) ) {
                    $this->_errorMsg = 'Please, check row: #' . $row . ' in the file. And try again by first step.';
                    $isOk = false;
                    break;
                }

                $temp[$row][] = $this->_api_data->status;
            }

            if ($isOk) {
                $this->_model->writeRows($temp, $mapping);
                $this->_model->transCommit();
            } else {
                $this->_model->transRollback();
            }
        } catch(Exception $e){
            $this->_model->transRollback();
        }

        return $isOk;
    }

    protected function _checkRequiredCells(&$row, $password) {
        foreach ($row as $key => &$item) {
            if (  '' == $item && !isset($this->notRequiredCells[$this->_api_data->mapping[$key]]) ) {
                return false;
            }

            if ('' == $item && 'password' == $this->_api_data->mapping[$key]) {
                $item = $password;
            }
        }
        return true;
    }
}