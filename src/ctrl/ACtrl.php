<?php

namespace ctrl;

class ACtrl extends \ctrl\Base
{
    protected $_upload_folder = null;

    function __construct() {
        parent::__construct();

        $this->_setUploadFolder();
    }

    public function _setUploadFolder($path = '/uploads/') {
        $this->_upload_folder = $path;
    }

    public function uploadFile() {
        $file_extension = null;
        $tempPath = $this->_renameTheFile($_FILES['file']['name'], $file_extension);

        if (!$tempPath) return false;

        $newPath = APP_ROOT . $this->_upload_folder . $tempPath;

        if ( 0 < $_FILES['file']['error'] ) return false;

        rename($_FILES['file']['tmp_name'], $newPath);

        $this->_apiData->sys_file_name = $newPath;
        $this->_apiData->file_name = $_FILES['file']['name'];
        $this->_apiData->password = $_REQUEST['password'];
        $this->_apiData->status = $_REQUEST['status'];
        $this->_apiData->file_extension = $file_extension;

        return true;
    }

    protected function _renameTheFile($fileName, &$extension) {
        $newName = time();
        $arr = explode('.', $fileName);
        $file_extension = $arr[count($arr) - 1];
        $extension = $file_extension;

        // TODO: In
        switch($file_extension) {
            case 'txt':
            case 'csv':
            case 'xlsx':
                break;
            default:
                return false;
        }

        if (count($arr) > 1)
            $newName .= '.' . $file_extension;
        return $newName;
    }
}