<?php

namespace ctrl;

class Step2 extends \ctrl\Base
{
    protected $_returnData = null;
    public function __construct($api_data)
    {
        $api_data->allowed_cols = [
            'null' => '',
            'firstname' => 'First Name',
            'lastname' => 'Last Name',
            'email' => 'Email',
            'country' => 'Country',
            'city' => 'City',
            'address' => 'Address',
            'password' => 'Password'
        ];

        switch ($api_data->file_extension) {
            case 'xlsx':
                $api_data->show_rows = $this->_getExcelData($api_data->sys_file_name, count($api_data->allowed_cols) - 1);
                break;

            default:
                $api_data->show_rows = $this->_getTxtData($api_data->sys_file_name);
                break;
        }

        $this->_returnData = $api_data;
    }

    public function getData() {
        return $this->_returnData;
    }

    protected function _getExcelData($file, $countCols) {
        $excelReader = \Singleton::getExcelReader($file);
        $excelObj = $excelReader->load($file);
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow() > 10 ? 3 : $worksheet->getHighestRow();

        $tempStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        $returnData = [];
        for ($row = 1; $row <= $lastRow; $row++) {
            $returnData[$row - 1] = [];

            for ($i = 0; $i < $countCols; $i++) {
                $returnData[$row - 1][] = $worksheet->getCell($tempStr[$i] . $row)->getValue();
            }
        }

        return $returnData;
    }
}