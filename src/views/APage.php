<?php

namespace views;

class APage
{
    protected $_styles = null;
    protected $_scripts = null;

    public function __construct() {
        $this->_scripts = $this->_getScripts();
        $this->_styles = $this->_getMainStyle();
    }

    protected function _getMainStyle() {
        return $this->_getAStyle('asset/main.css');
    }

    protected function _getAScript($path) {
        if (!isset($path)) {
            die_by_error(__FILE__, __LINE__, 'No path');
        }

        return "<script src=\"{$path}\"></script>";
    }

    protected function _getAStyle($path) {
        if (!isset($path)) {
            die_by_error(__FILE__, __LINE__, 'No path');
        }

        return "<link rel=\"stylesheet\" href=\"{$path}\">";
    }

    protected function _getScripts() {

        return $this->_getAScript("vendor/angular.min.js")
            . $this->_getAScript("vendor/angular-route.min.js")
            . $this->_getAScript("vendor/angular-cookies.min.js")
            . $this->_getAScript("asset/main.js")
            . $this->_getAScript("asset/first-ctrl.js")
            . $this->_getAScript("asset/second-ctrl.js")
            . $this->_getAScript("asset/third-ctrl.js");
    }

//    protected function _getTextIntoFile($path) {
//        if (!isset($path)) {
//            die_by_error(__FILE__, __LINE__, 'No path');
//        }
//
//        $str = '';
//        $f = fopen(APP_ROOT . '/tmp/' . $path, "r");
//        while(!feof($f))
//            $str .= fgets($f);
//        fclose($f);
//        return $str;
//    }

    public function getHtml() {
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <title>Upload Employees</title>
    <link rel=\"stylesheet\" href=\"vendor/bootstrap.min.css\">
    {$this->_styles}
</head>
<body>
<figure ng-app='myApp' class=\"my-content\">
<ng-view></ng-view>
</figure>
{$this->_scripts}
</body>
</html>";
    }
}