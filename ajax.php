<?php
require_once 'app.php';

$response = new stdClass();

switch ($_REQUEST['action']) {
    case 'upload_file':
        $file = new \ctrl\ACtrl();
        $file->uploadFile();
        $response->api_data = $file->getApiData();
        $response->status = true;
        break;
    case 'get_data_step2':
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $step2 = new \ctrl\Step2($request);
        $response->api_data = $step2->getData();
        $response->status = true;
        break;
    case 'write_from_temp':
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);

        $step3 = new \ctrl\Step3($request);
        $response->status = $step3->writeDataToDb();
        if (!$response->status) {
            $response->msg = $step3->getErrorMsg();
        }
        break;
    default:
        break;
}

if (!isset($response->status))
    $response->status = 'false';

echo json_encode($response);