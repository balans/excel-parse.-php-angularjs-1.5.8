CREATE TABLE `employees` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
 `added_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 `firstname` varchar(255) DEFAULT NULL,
 `lastname` varchar(255) DEFAULT NULL,
 `email` varchar(255) DEFAULT NULL,
 `country` varchar(255) DEFAULT NULL,
 `city` varchar(255) DEFAULT NULL,
 `address` varchar(255) DEFAULT NULL,
 `status` varchar(48) DEFAULT NULL,
 `password` varchar(255) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;