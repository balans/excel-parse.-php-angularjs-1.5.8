app.controller('uploader', ['$scope', 'multipartForm', '$cookies', '$rootScope',
    function($scope, multipartForm, $cookies, $rootScope) {
        $rootScope.cookies = $cookies;

        $rootScope.api_data = null;
        $scope.cookies.put('api_data', null);

        $scope.password = '';
        $scope.status = 'null';
        $scope.customer = {};

        $scope.upload = function() {
            var path = 'ajax.php?'
                + 'action=upload_file'
                + '&password=' + $scope.password
                + '&status=' + $scope.status;

            multipartForm.post($rootScope, path, $scope.customer);
        };

        $scope.cancel = function() {
            location.href = location.origin + location.pathname;
        };
    }]);

app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse( attrs.fileModel );
            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

app.service('multipartForm', ['$http', function($http){
    this.post = function($scope, uploadUrl, data) {
        console.log($scope)

        if ('undefined' == typeof data.file) {
            alert('Sorry! But system have to have a file for continued.\n' +
                'Please choose a file');
            return false;
        }

        var fd = new FormData();
        for (var key in data)
            fd.append(key, data[key]);

        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function(msg) {
                if( true === msg.data.status ) {
                    $scope.api_data = msg.data.api_data;
                    $scope.cookies.put('api_data', JSON.stringify(msg.data.api_data) );
                    location.href = location.origin + location.pathname + '#/second';
                } else {
                    alert( 'Sorry, but allowed files are: .csv , .txt , xlsx' );
                }
            },
            function() {
                alert( 'Sorry, but uploading did not work.\nPlease, try again later.' )
            }
        );
    }
}]);