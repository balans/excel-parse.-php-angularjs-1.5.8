app.controller('step3-ctrl', ['$scope', 'ajaxData', 'writeFromFile', '$cookies', '$rootScope', function ($scope, ajaxData, writeFromFile, $cookies, $rootScope) {
    $scope.cookies = $cookies;
    if( 'undefined' == typeof $scope.api_data ) {
        var api_data = $cookies.get('api_data');

        api_data = 'undefined' != typeof api_data && '' != api_data
            ? JSON.parse(api_data) : null;

        if (!api_data || !api_data.file_name)
            $scope.cancel();

        $rootScope.api_data = api_data;
    }

    ajaxData.getAjaxData($rootScope);

    $scope.back = function() {
        location.href = location.origin + location.pathname + '#/second';
    };

    $scope.save = function() {
        writeFromFile.writeData($scope);
    };

    $scope.cancel = function() {
        location.href = location.origin + location.pathname;
    };
}]);

app.service('writeFromFile', ['$http', function($http){
    $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

    this.writeData = function($scope) {
        $http({
            method: 'POST',
            url: 'ajax.php?action=write_from_temp',
            data: $scope.api_data
        }).then(function successCallback(response) {
            if(response.data.status) {
                location.href = location.origin + location.pathname;
            } else {
                alert(response.data.msg);
            }
        }, function errorCallback(response) {
        });
    }
}]);
