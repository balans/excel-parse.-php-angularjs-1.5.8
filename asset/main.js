var app = angular.module('myApp', ['ngRoute', 'ngCookies']);

app.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'tmp/first.html'
        })
        .when('/second', {
            templateUrl: 'tmp/second.html'
        })
        .when('/third', {
            templateUrl: 'tmp/third.html'
        })
        .otherwise({
            templateUrl: 'tmp/404.html'
        });
});

app.service('ajaxData', ['$http', function($http){
    $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

    this.getAjaxData = function($scope) {
        $http({
            method: 'POST',
            url: 'ajax.php?action=get_data_step2',
            data: $scope.api_data
        }).then(function successCallback(response) {
            $scope.api_data = response.data.api_data;
        }, function errorCallback(response) {
        });
    }
}]);