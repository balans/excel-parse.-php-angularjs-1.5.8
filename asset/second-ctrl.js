app.controller('step2', ['$scope', 'ajaxData', '$cookies', '$rootScope',
    function ($scope, ajaxData, $cookies, $rootScope) {
        $scope.cookies = $cookies;
        if( 'undefined' == typeof $scope.api_data ) {
            var api_data = $cookies.get('api_data');

            api_data = 'undefined' != typeof api_data && '' != api_data
                ? JSON.parse(api_data) : null;

            if (!api_data || !api_data.file_name)
                $scope.cancel();

            $rootScope.api_data = api_data;
        }

        ajaxData.getAjaxData($rootScope);

        $scope.cancel = function() {
            location.href = location.href = location.origin + location.pathname;
        };

        $scope.show_preview = function() {
            $scope.cookies.put('api_data', JSON.stringify($scope.api_data) );

            if ('null' == $scope.api_data.status
                || null === $scope.api_data.password
                || '' == $scope.api_data.password) {
                alert('Sorry! But Status and Password can not be empty');
                return false;
            }

            var msg = 'Sorry! But system need to have all columns';

            if (
                'undefined' == typeof $scope.api_data.mapping
                || null === $scope.api_data.mapping
                || $scope.api_data.mapping.length != $scope.api_data.show_rows[0].length
            ) {
                alert(msg);
                return false;
            }

            for (var key in $scope.api_data.mapping) {
                if ( $scope.api_data.mapping[key] === null ) {
                    alert(msg);
                    return false;
                }
            }

            location.href = location.origin + location.pathname + '#/third';
        };
    }]);

app.directive('selectColumns', function () {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var $scope = scope;

            setTimeout(function() {
                if ( 'null' != $scope.api_data.allowed_cols
                    && $scope.api_data.mapping
                    && 'undefined' != typeof $scope.api_data.mapping[attrs.numb]) {
                    element.val($scope.api_data.mapping[attrs.numb]);
                }
            }, 100);

            element.on('change', function(){
                var value = element.val(),
                    isUniqValue = true;

                if (null === $scope.api_data.mapping) {
                    $scope.api_data.mapping = [];
                }

                var i = -1;

                if ('null' != value)
                    while ('undefined' != typeof $scope.api_data.mapping[++i]) {
                        if ($scope.api_data.mapping[i] == value) {
                            isUniqValue = false;
                            break;
                        }
                    }

                if (!isUniqValue) {
                    element.val('null');
                    alert('Value of Select have to be is unique. Please choose other value.');
                    return false;
                } else {
                    $scope.api_data.mapping[attrs.numb] = value;
                    $scope.cookies.put('api_data', JSON.stringify($scope.api_data) );
                }
            });
        }
    };
});